package com.example.calcmelvincapron.calculate;

import com.example.calcmelvincapron.calculate.interfaces.OperatorInterface;

public class Multiply implements OperatorInterface {
    @Override
    public float calculate(float first, float second) {
        return first * second;
    }
}
