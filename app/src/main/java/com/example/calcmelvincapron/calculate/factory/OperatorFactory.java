package com.example.calcmelvincapron.calculate.factory;

import com.example.calcmelvincapron.calculate.Divide;
import com.example.calcmelvincapron.calculate.Minus;
import com.example.calcmelvincapron.calculate.Multiply;
import com.example.calcmelvincapron.calculate.Plus;
import com.example.calcmelvincapron.calculate.interfaces.OperatorInterface;

public class OperatorFactory {
    private String type;

    /**
     * @param type of the operator needed
     */
    public OperatorFactory(String type) {
        this.type = type;
    }

    /**
     * @return OperatorInterface - an operator
     */
    public OperatorInterface create() {
        OperatorInterface operator = null;

        switch (type) {
            case "Plus":
                operator = new Plus();
                break;
            case "Minus":
                operator = new Minus();
                break;
            case "Multiply":
                operator = new Multiply();
                break;
            case "Divide":
                operator = new Divide();
                break;
        }

        return operator;
    }
}
