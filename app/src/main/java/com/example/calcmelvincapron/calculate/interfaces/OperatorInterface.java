package com.example.calcmelvincapron.calculate.interfaces;

public interface OperatorInterface {
    /**
     *
     * @param first The first value to calculate
     * @param second The second value to calculate
     * @return float
     */
    float calculate(float first, float second);
}
