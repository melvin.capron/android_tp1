package com.example.calcmelvincapron;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.calcmelvincapron.calculate.factory.OperatorFactory;
import com.example.calcmelvincapron.calculate.interfaces.OperatorInterface;

public class MainCalc extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initApp();
    }

    /**
     * Init the application
     */
    private void initApp() {
        this.handleRazButton();
        this.handleCalculateButton();
        this.handleExitButton();
    }

    /**
     * Handle event on RAZ button
     */
    private void handleRazButton() {
        Button clickButton = findViewById(R.id.raz);
        clickButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((EditText) findViewById(R.id.firstValue)).setText(null);
                ((EditText) findViewById(R.id.secondValue)).setText(null);
            }
        });
    }

    /**
     * Handle event on exit button
     */
    private void handleExitButton() {
        Button clickButton = findViewById(R.id.leave);
        clickButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });
    }

    /**
     * Handle event on calculate button
     */
    private void handleCalculateButton() {
        final MainCalc that = this;
        Button clickButton = findViewById(R.id.calculate);
        clickButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                float firstValue;
                float secondValue;

                try {
                    firstValue = that.getValueFromTextView(R.id.firstValue);
                    secondValue = that.getValueFromTextView(R.id.secondValue);
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Please fill up values", Toast.LENGTH_SHORT).show();
                    return;
                }

                String operatorRadio = that.getOperatorFromRadioButtons();

                OperatorInterface operator = (new OperatorFactory(operatorRadio)).create();
                float result = operator.calculate(firstValue, secondValue);

                that.showResult(result);
            }
        });
    }

    /**
     * @return operator selected by buttons
     */
    private String getOperatorFromRadioButtons() {
        RadioGroup radioGroup = findViewById(R.id.operatorsGroup);
        int radioButtonID = radioGroup.getCheckedRadioButtonId();
        RadioButton radioButton = radioGroup.findViewById(radioButtonID);

        return (String) radioButton.getText();
    }

    /**
     * @param result of preceding operation
     */
    private void showResult(float result) {
        TextView resultTextView = findViewById(R.id.result);
        resultTextView.setText(String.valueOf(result));
    }

    /**
     * Extract any value from any EditText by a given ID
     * @param id of the EditText
     * @return value of the EditText
     * @throws Exception if the string is empty
     */
    private float getValueFromTextView(int id) throws Exception {
        String value = ((EditText) findViewById(id)).getText().toString();

        if (value.isEmpty()) {
            throw new Exception("String is empty");
        }

        return Float.parseFloat(value);
    }
}